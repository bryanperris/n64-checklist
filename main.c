#include <stdio.h>
#include <libdragon.h>

int spWorked = 0;
int viWorked = 0;
int dpWorked = 0;
int siWorked = 0;

int rsp_program[] = {
    0x00000000,
    0x0000000D
};

void
__sp_handler() {
    spWorked = 1;
}

void
__vi_handler() {
    viWorked = 1;
}

void
__dp_handler() {
    dpWorked = 1;
}

void __si_handler() {
    siWorked = 1;
}

int 
main(void)
{
    /* Init interrupts */
    init_interrupts();

    /* Init the signal processor */
    rsp_init();

    /* Init the graphics rasterizer */
    rdp_init();

    /* Init controller */
    controller_init();

    /* Register interrupt call backs */
    register_SP_handler(__sp_handler);
    register_VI_handler(__vi_handler);
    register_DP_handler(__dp_handler);
    register_SI_handler(__si_handler);

    /* Setup the display */
    console_init();
    console_set_render_mode(RENDER_MANUAL);
    console_clear();

    set_SP_interrupt(1);
    set_SI_interrupt(1);

    load_ucode(rsp_program, 8);
    run_ucode();

    rdp_sync(SYNC_FULL);

    //get_controllers_present();

    while (1) {
        static int count = 0;
        console_clear();
        printf("Interrupts: %i\n", get_interrupts_state());
        printf("VI Interrupt %i\n", viWorked);
        printf("SP Interrupt %i\n", spWorked);
        printf("DP Interrupt %i\n", dpWorked);
        printf("SI Interrupt %i\n", siWorked);
        console_render();

        for (int i = 0; i < 1000; i++) ;
    }
}